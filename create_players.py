from random import randint, uniform, randrange
import json
from datetime import datetime, timedelta

def random_date(start, end):
    delta = end - start
    int_delta = (delta.days * 24 * 60 * 60) + delta.seconds
    random_second = randrange(int_delta)
    return start + timedelta(seconds=random_second)

def create_json_data(data, total):
    data["players"] = []
    d1 = datetime.strptime('1/1/2008 1:30 PM', '%m/%d/%Y %I:%M %p')
    d2 = datetime.strptime('12/04/2019 4:50 AM', '%m/%d/%Y %I:%M %p')
    for x in range(1, (total + 1)):
        total_matches = randint(1, 1000)
        total_wins = randint(1, total_matches)
        total_losses = total_matches - total_wins
        total_kills = randint(0, (total_wins * randint(0, 5) + total_losses * randint(0, 4)))
        total_deaths = total_losses + int(uniform(0, 1) * total_wins)
        total_shots_fired = total_matches * randint(1, 100)
        total_shots_hit = randint(0, total_shots_fired)
        total_shots_missed = total_shots_fired - total_shots_hit
        last_time_played = str(random_date(d1, d2))
        data["players"].append({'id': x, 'total matches': total_matches, 'total wins': total_wins, 'total losses': total_losses, 'total kills': total_kills, 'total deaths': total_deaths, 'total shots fired': total_shots_fired, 'total shots hit': total_shots_hit, 'total shots missed': total_shots_missed, 'last time played': last_time_played})

if __name__ == "__main__":
    data = {}
    create_json_data(data, 10000)
    with open("players.json", 'w') as outfile:
        json.dump(data, outfile, indent = 4)