import time
import random
import os
import subprocess

# We maken lege arrays aan voor de informatie van de cluster
player_ids = []
player_skills = []

# We lezen de cluster in en voegen de informatie toe aan de arrays
def read_cluster():
        global player_ids
        global player_skills
        player_ids = []
        player_skills = []
	try:
	    cat = subprocess.Popen(["hdfs", "dfs", "-cat", "cluster.csv"], stdout=subprocess.PIPE)
	    counter = 0
    	    for line in cat.stdout:
    		if counter > 0:
    		    arr = line.split(",")
    		    player_ids.append(arr[1])
    		    player_skills.append(arr[2])
    		else:
    		    counter += 1

	except Exception as e:
	    pass
	
read_cluster()

# We genereren random matchdata voor een speler
def get_data():
	wins = random.randint(0, 1)
	losses = 1 - wins
	kills = random.randint(0, 5)
	deaths = random.randint(0, 5)
	hits = random.randint(0, 90)
	misses = random.randint(0, (90 - hits))
	return str(wins) + ";" + str(losses) + ";" + str(kills) + ";" + str(deaths) + ";" + str(hits) + ";" + str(misses)

# het path waar onze matchdata naartoe wordt geschreven
path = "matches/"
counter = 1
while True:
	# Elke keer gaan we kijken of er een nieuwe cluster is gemaakt en indien die gemaakt is gaan we die uitlezen
	read_cluster()
	# we maken een string dat uiteindelijk onze csv file met de matchdata gaat zijn
	csv_string = ""
	file_name = "match_" + str(counter) + ".csv"
	players = []
	# We kiezen onze eerste speler en voegen hem toe aan de match
	player_id = random.randint(1, 100)
	players.append(player_id)
	# Hier kijken we of de gekozen speler al een skillscore toegewezen heeft gekregen, anders geven we standaard 0
	target_skill = 0
	if player_id in player_ids:
		i = player_ids.index(player_id)
		target_skill = player_skills[i]
	total_players = 1
	# We gaan 10 keer proberen om een speler met dezelfde skillscore aan de match toe te voegen, na 10 keer voegen we random spelers toe
	attempts = 10
	while total_players < 11:
		player_id = random.randint(1, 100)
		attempts -= 0
		if player_id not in players:
			if attempts <= 0:
				data = get_data()
				csv_string += str(player_id) + ";" + data + "\n"
				total_players += 1
			player_skill = 0
			if player_id in player_ids:
				i = player_ids.index(player_id)
				player_skill = player_skills[i]
			if player_skill == target_skill:
				data = get_data()
				csv_string += str(player_id) + ";" + data + "\n"
				total_players += 1

	# We schrijven de matchdata weg naar een csv-bestand 
	os.system('hdfs dfs -touchz "%s"' %(path + file_name))
	os.system('echo "%s" | hdfs dfs -appendToFile - "%s"' %(csv_string,(path + file_name)))
	counter += 1
	time.sleep(2)