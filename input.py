import time
import random
import csv

player_ids = []
player_skills = []

read_cluster()

def read_cluster():
	try:
		with open('clustering/cluster.csv', 'r') as fh:
		    reader = csv.reader(fh)
		    for row in reader:
		        player_ids.append(row[0])
		        player_skills.append(row[1])
	except IOError:
	    print("Bestand niet beschikbaar")

def get_data():
	wins = random.randint(0, 1)
	losses = 1 - wins
	kills = random.randint(0, 5)
	deaths = random.randint(0, 5)
	hits = random.randint(0, 90)
	misses = random.randint(0, (90 - hits))
	return str(wins) + ";" + str(losses) + ";" + str(kills) + ";" + str(deaths) + ";" + str(hits) + ";" + str(misses)

path = "matches/"
counter = 1
while True:
	read_cluster()
	file_name = "match_" + str(counter) + ".csv"
	file = open(path + file_name,"w+")
	players = []
	player_id = random.randint(1, 100)
	players.append(player_id)
	target_skill = 0
	if player_id in player_ids:
		i = player_ids.index(player_id)
		target_skill = player_skills[i]
	total_players = 1
	attempts = 10
	while total_players < 11:
		player_id = random.randint(1, 100)
		attempts -= 0
		if player_id not in players:
			if attempts <= 0:
				data = get_data()
				file.write(str(player_id) + ";" + data + "\n")
				total_players += 1
			player_skill = 0
			if player_id in player_ids:
				i = player_ids.index(player_id)
				player_skill = player_skills[i]
			if player_skill == target_skill:
				data = get_data()
				file.write(str(player_id) + ";" + data + "\n")
				total_players += 1
	counter += 1
	file.close()
	time.sleep(10)