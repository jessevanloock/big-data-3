import pandas as pd
from sklearn.datasets.samples_generator import make_blobs
from sklearn.cluster import KMeans
from datetime import datetime

# import csv
data_file_name = str(datetime.today().strftime('%Y-%m-%d')) + ".csv"
df = pd.read_csv(('../tmp/' + data_file_name), index_col=1)
df = df.iloc[:,1:]
print(df)

# add weights
df['w_wins'] = (df.wins / (df.losses + 1)) * 0.5
df['w_kills'] = (df.kills / (df.deaths + 1)) * 0.3
df['w_hits'] = ((df.hits / (df.misses + 1)) / 10) * 0.2

kmeans = KMeans(n_clusters=4)

y = kmeans.fit_predict(df[['w_wins', 'w_kills', 'w_hits']])

df['Cluster'] = y

print(df)
df.to_csv("cluster.csv", columns=['Cluster'], sep=',')