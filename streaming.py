from pyspark.sql import SparkSession
from pyspark.sql.functions import explode
from pyspark.sql.functions import split
import pyspark.sql.functions as f
from pyspark import SparkContext
from pyspark.sql.types import IntegerType, DoubleType, StringType, StructType, StructField
from pandas import Series, DataFrame
from datetime import datetime
import pandas as pd
from sklearn.cluster import KMeans


todays_date = str(datetime.today().strftime('%H_%M'))

sc =SparkContext()
sess = SparkSession(sc)

spark = SparkSession \
    .builder \
    .appName("Matchmaking") \
    .getOrCreate()
    
userSchema = StructType() \
	.add("player_id", "string") \
	.add("wins", "integer") \
    .add("losses", "integer") \
	.add("kills", "integer") \
	.add("deaths", "integer") \
	.add("hits", "integer") \
	.add("misses", "integer")

csvDF = spark \
    .readStream \
    .option("sep", ";") \
    .schema(userSchema) \
    .csv("matches")

csvDF.printSchema()

def createCluster():
    # import csv
    data_file_name = todays_date + ".csv"
    df = pd.read_csv(('tmp/' + data_file_name), index_col=1)
    df = df.iloc[:,1:]
    print(df)

    # add weights
    df['w_wins'] = (df.wins / (df.losses + 1)) * 0.5
    df['w_kills'] = (df.kills / (df.deaths + 1)) * 0.3
    df['w_hits'] = ((df.hits / (df.misses + 1)) / 10) * 0.2

    kmeans = KMeans(n_clusters=4)

    y = kmeans.fit_predict(df[['w_wins', 'w_kills', 'w_hits']])

    df['Cluster'] = y

    print(df)
    df.to_csv("clustering/cluster.csv", columns=['Cluster'], sep=',')

def writeToFile(df, epochId):
    global todays_date
    data_file_name = todays_date + ".csv"
    df.toPandas().to_csv(("tmp/" + data_file_name), header=True)
    if str(datetime.today().strftime('%H_%M')) != todays_date:
        createCluster()
        todays_date = str(datetime.today().strftime('%H_%M'))


playerCounts = csvDF.groupby("player_id").agg(f.sum('wins').alias('wins'),f.sum('losses').alias('losses'),f.sum('kills').alias('kills'),f.sum('deaths').alias('deaths'),f.sum('hits').alias('hits'),f.sum('misses').alias('misses'))

query = playerCounts \
    .writeStream \
    .foreachBatch(writeToFile) \
    .outputMode("complete") \
    .start()

query.awaitTermination()