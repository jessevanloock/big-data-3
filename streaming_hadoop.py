from pyspark.sql import SparkSession
from pyspark.sql.functions import explode
from pyspark.sql.functions import split
import pyspark.sql.functions as f
from pyspark import SparkContext
from pyspark.sql.types import IntegerType, DoubleType, StringType, StructType, StructField
from pandas import Series, DataFrame
from datetime import datetime
import pandas as pd
import subprocess
from sklearn.cluster import KMeans
import os
import io
from io import StringIO
import csv

# We houden de dag bij zodat we bij een nieuwe dag opnieuw clustering kunnen doen
todays_date = str(datetime.today().strftime('%H_%M'))

# Hier maken we een sparksessie aan met de naam 'Matchmaking'
spark = SparkSession \
    .builder \
    .appName("Matchmaking") \
    .getOrCreate()
    
# We maken de structuur aan van de binnenkomende data
userSchema = StructType() \
    .add("player_id", "string") \
    .add("wins", "integer") \
    .add("losses", "integer") \
    .add("kills", "integer") \
    .add("deaths", "integer") \
    .add("hits", "integer") \
    .add("misses", "integer")

# Hier lezen we de binnenkomende data in en dit zijn csv files in de map matches die gescheiden zijn met ';'
csvDF = spark \
    .readStream \
    .option("sep", ";") \
    .schema(userSchema) \
    .csv("matches")

# In deze functie gaan we onze aggregated data lezen en hierop clustering uitvoeren
def createCluster(file_path):
    # import csv
    data_file_path = "data/" + str(file_path) + "/"
    # csv-bestand inlezen
    cat = subprocess.Popen(["hdfs", "dfs", "-cat", data_file_path + "*.csv"], stdout=subprocess.PIPE)
    csv_string = "player_id;wins;losses;kills;deaths;hits;misses\n"
    for line in cat.stdout:
       csv_string += line
    print(csv_string)

    # Zet string om naar csv
    f = StringIO(unicode(csv_string))

    # Maak dataframe op basis van csv
    df = pd.read_csv(f, sep=';')
    print(df)

    # Gewichten toevoegen
    df['w_wins'] = (df.wins / (df.losses + 1)) * 0.5
    df['w_kills'] = (df.kills / (df.deaths + 1)) * 0.3
    df['w_hits'] = ((df.hits / (df.misses + 1)) / 10) * 0.2

    # Aantal clusters kiezen
    kmeans = KMeans(n_clusters=4)
    # Kiezen waarop de cluster gebaseerd is
    y = kmeans.fit_predict(df[['w_wins', 'w_kills', 'w_hits']])
    # Resultaat cluster aan dataframe toevoegen
    df['Cluster'] = y

    # Hier gaan we onze dataframe omzetten naar een csv bestand en deze opslaan in hdfs
    s = io.StringIO()
    df.to_csv(unicode(s))
    print(s.getvalue())
    os.system('hdfs dfs -rmr "%s"' %("cluster.csv"))
    os.system('hdfs dfs -touchz "%s"' %("cluster.csv"))
    os.system('echo "%s" | hdfs dfs -appendToFile - "%s"' %(df.to_csv(columns=['player_id','Cluster'], sep=','),"cluster.csv"))


# Hier wordt de huidige dataframe opgeslagen in hdfs en wordt gecheckt of een nieuwe dag is gestart om zo clustering te doen
def writeToFile(df, epochId):
    global todays_date
    dfpath = "hdfs:///user/hadoop/data/" + str(epochId)
    df.coalesce(1).write.save(path=dfpath, format='csv', sep=';')
    if str(datetime.today().strftime('%H_%M')) != todays_date and epochId > 0:
        createCluster(epochId - 1)
        todays_date = str(datetime.today().strftime('%H_%M'))

# Hier wordt de structuur van onze streamingoutput gemaakt en geaggregeerd
playerCounts = csvDF.groupby("player_id").agg(f.sum('wins').alias('wins'),f.sum('losses').alias('losses'),f.sum('kills').alias('kills'),f.sum('deaths').alias('deaths'),f.sum('hits').alias('hits'),f.sum('misses').alias('misses'))

# De query schrijft elke batch de output weg naar hdfs
query = playerCounts \
    .writeStream \
    .foreachBatch(writeToFile) \
    .outputMode("complete") \
    .start()

query.awaitTermination()